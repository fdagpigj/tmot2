This file describes how to use the The Mods of Tolagal version 2.0 (TMOT2) patcher to install compatible mods for The Depths of Tolagal. This software is entirely fan made and unofficial. Run at your own risk. TMOT2 is not compatible with the original TMOT.


=== Folders ===

This folder (TMOT2) should be a folder located within your game folder (the one that contains the game executable, most likely called The Depths of Tolagal) and you should run the installer from here. All mods go as subfolders in the mods/ folder. A mod folder will usually contain at least an appends/ folder and a few .txt files. The name of the mod folder is what you put in modlist.txt.

To clarify, here's a simplified illustration of what your folder structure might look like:

The Depths of Tolagal/
  ..index.html
  ..TMOT2/
    ..modlist.txt
    ..TMOT.sh
    ..mods/
      ..base/
        ..provides.txt
      ..yourmod/
        ..provides.txt


=== Selecting mods to install ===

Put the names of the mods you want to install in modlist.txt, in the order you want them to be installed. modlist.txt should contain the name of one mod per line, and no extra blankspace is tolerated. If the modlist.txt file is missing or empty, the installer will simply restore the backups and not do anything else. Any lines starting with a hash character (#) are considered comments and ignored. Some mods might depend on other mods to have been installed before them, and if their dependencies aren't met, then the installer will exit instantly, without restoring backups again.


=== Backups ===

The installer will automatically create a backup when modifying a file if it is not yet backed up. Backups are stored under the TMOT2/backups/ folder, which gets created automatically. If these backups ever get messed up, you can simply delete the entire backups folder and check integrity of local files in steam to restore the unmodded files. The installer will always restore any existing backups before starting to install any mods, so you'll also need to delete them if the game gets an update. You might also need to do this in some cases if a mod updates, because files added by mods also get backed up.


=== Running the installer ===

When you've read this readme and are ready to run the patcher:

If you have bash (probably do if you're on gnu/linux or mac) simply open a bash shell in this folder and run 
./TMOT.sh

If you're on Windows, simply double-click the run.bat file.

After running the installer, make sure to read through the console output to check for errors/warnings. If something doesn't look right and you don't know how to fix it yourself, take a screenshot and post on the forum thread.

While the windows version should work, I haven't tested it quite as thoroughly. If you encounter problems, you could try to get bash and run the bash version.
