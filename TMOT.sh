#!/bin/bash

#file saying which mods to install
MODLIST="modlist.txt"
#file in a mod saying which other mods it depends on
DEPENDS="requires.txt"
#file in a mod saying which dependencies this mod will fulfill
PROVIDES="provides.txt"
#directory containing files that will overwrite any such existing files
OVERWRITES="overwrites/"
#directory containing files that will be appended to any such existing files
APPENDS="appends/"
#directory for backups of all modified files
BACKUPS="backups/"
#directory wherein each mod is a subfolder
MODS="mods/"
#actual game directory
ORIG="../"
#version info
PACKAGE_JSON="package.json"
VERSION_INFO="recommended_version.txt"

echo "Running..."

if [ ! -d "$BACKUPS" ]; then
	mkdir "$BACKUPS"
else
	#restore backups before proceeding
	if [ -e "$BACKUPS" ] && [ -n "$(ls -A $BACKUPS)" ]; then
		echo "Restoring old backups..."
		if cp -r "$BACKUPS"* "$ORIG"; then
			echo "Done restoring backups."
		else
			echo "Unknown Error restoring backups. Exiting"
			exit 2
		fi
	fi
fi
if [ ! -f "$MODLIST" ]; then
	echo "$MODLIST not found. Exiting."
	exit 0
fi
det_ver="none"
[ -f "$ORIG$PACKAGE_JSON" ] && det_ver=$(grep -e "version" "$ORIG$PACKAGE_JSON") && det_ver=${det_ver#*"version\":"*"\""} && det_ver=${det_ver%%\"*} && echo "Detected game version $det_ver"

create_backup() {
	if [ ! -e "$BACKUPS$1" ]; then
		if [ -f "$ORIG$1" ]; then
			#echo "backing up $1"
			dir="${1%/*}"
			[ "$dir" != "$1" ] && mkdir -p "$BACKUPS$dir"
			cp "$ORIG$1" "$BACKUPS$1"
		#else
		#	echo "would've backed up $1 but it does not exist"
		fi
	#else
	#	echo "would've backed up $1 but it already is backed up"
	fi
}

#associative array of resources provided by mods installed thus far
#..which other mods may depend on
declare -A MODARRAY

while read -u 10 modname; do
	#skip lines starting with #
	[[ "$modname" = \#* || -z "$modname" ]] &&continue
	#mods prefixed with ? are installed if present
	required=1 && [[ "$modname" = \?* ]] && required=0 && modname=${modname#?}
	echo "attempting to install mod: $modname"
	# check presence
	if [ ! -d "$MODS$modname" ]; then
		if [ -f "$MODS$modname.zip" ]; then
			# unzip
			echo "..unzipping $modname"
			if unzip -d "$MODS" "$MODS$modname.zip"; then
				echo "..unzipped successfully"
			else
				echo "Unknown Error unzipping. Exiting"
				exit 4
			fi
		else
			if [ $required == 1 ]; then
				echo "Error: mod folder/zip for $modname (not optional) not found. Exiting"
				exit 1
			else
				echo "Mod folder/zip for $modname (marked optional) not found. Skipping"
				continue
			fi
		fi
	fi
	# install
	if [ -d "$MODS$modname" ]; then
		modpath="$MODS$modname/"
		# check version
		if [ -f "$modpath$VERSION_INFO" ]; then
			rec_ver=$(cat "$modpath$VERSION_INFO")
			if [ $rec_ver != $det_ver ]; then
				echo "Warning: $modname recommends version $rec_ver but $det_ver was detected!"
			fi
		fi
		# check depends
		if [ -f "$modpath$DEPENDS" ]; then
			errormsg=""
			anyaltmet=0
			while read -u 11 reqname; do
				[[ "$reqname" = \#* ]] &&continue
				[[ "$reqname" = :* ]] && errormsg=${reqname#?} &&continue
				alt=0 && [[ "$reqname" = \?* ]] && alt=1 && reqname=${reqname#?}
				inverse=0 && [[ "$reqname" = \!* ]] && inverse=1 && reqname=${reqname#?}
				satisfied=0
				exists=0 && [[ ${MODARRAY["$reqname"]} ]] && exists=1
				[[ $inverse != $exists ]] && satisfied=1 && anyaltmet=1
				if [ $satisfied == 0 ]; then
					if [ $alt == 0 ] && [ $anyaltmet == 0 ]; then
						if [ $inverse == 0 ]; then
							echo "Error: Requirement $reqname not met. Exiting"
							[ -n "$errormsg" ] && echo "$errormsg"
							exit 3
						else
							echo "Error: Conflicting resource $reqname loaded. Exiting"
							[ -n "$errormsg" ] && echo "$errormsg"
							exit 3
						fi
					fi
				fi
				[ $alt == 0 ] && anyaltmet=0 #&& echo "DEBUG: not alternative, resetting anyaltmet"
			done 11<$modpath$DEPENDS
		fi
		shopt -s globstar
		# overwrites
		for filepath in "$modpath$OVERWRITES"**/*; do
			[ -f "$filepath" ] ||continue
			filename="${filepath#$MODS*/$OVERWRITES}"
			create_backup "$filename"
			# make an empty file if it does not exist
			if [ ! -f "$ORIG$filename" ]; then
				mkdir -p "$ORIG${filename%/*}"
				touch "$ORIG$filename"
			fi
			# really overwrite
			cp "$filepath" "$ORIG$filename"
		done
		# appends
		for filepath in "$modpath$APPENDS"**/*; do
			[ -f "$filepath" ] ||continue
			filename="${filepath#$MODS*/$APPENDS}"
			create_backup "$filename"
			# make an empty file if it does not exist
			if [ ! -f "$ORIG$filename" ]; then
				mkdir -p "$ORIG${filename%/*}"
				touch "$ORIG$filename"
			fi
			# really append
			cat "$filepath" >> "$ORIG$filename"
		done
		# add provides to array
		if [ ! -f $modpath$PROVIDES ]; then
			echo "Warning: $modname provides nothing!"
		else
			while read -u 11 reqname; do
				[[ "$reqname" = \#* ]] &&continue
				MODARRAY["$reqname"]=1
			done 11<$modpath$PROVIDES
		fi
		echo "finished installing mod: $modname"
	else
		echo "Unknown Error"
	fi
done 10<$MODLIST

echo "Done. Exiting."
