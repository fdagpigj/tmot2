This file is a guide for anyone interested in making their own mod compatible with TMOT v2. To see how a mod might actually look codewise, I can only recommend you look at fmod2. If you're making a mod, I recommend enabling the toolbar in package.json so you can open a console for debugging, and unminifying game.min.js using a javascript beautifier.


Your mod should be a folder under mods/ with the name of your mod. Please avoid using special characters and spaces in the name of the folder. They might work but even if they do, other people might find them annoying.


# Mod contents

Your mod folder can contain 5 things that will be recognised by TMOT. None of these is strictly required:

-an overwrites/ folder, containing any files that your mod will completely overwrite. I think most mods should not need this, but it's there in case you find a use for it, such as replacing the app-icon.png. The file hierarchy here should be the same as in the game folder (ie. game.min.js will be at the top level, directly inside this folder).

-an appends/ folder, containing any files that your mod will append with extra content. This is where I think most of your mod contents will go. Like with overwrites/, the file hierarchy should be the same as within the game folder.

-a provides.txt file, which will contain a bunch of words that other mods can depend upon. Please put at least something here, and if a new version of your mod is significantly different, consider changing the contents of this file in a predictable manner.

-a requires.txt file, which will contain a bunch of words that should or should not appear in the provides.txt of other installed mods. If any dependency listed is not met, the installer will instantly exit with an error and the mod will not be installed.

-a recommended_version.txt file, which will contain nothing but the game version string that your mod is intended for (as seen in package.json). If the installer detects a different version, it will produce a warning.


# Provides and requires

The contents of provides.txt and requires.txt should be one word per line. Empty lines and other whitespace are not supported. Please avoid using special characters here. Any line beginning with a hash (#) will be considered a comment and ignored. A dependency is considered to be met if any mod that was loaded before it provided the necessary word.

In addition, in requires.txt:

-any line starting with a question mark (?) will be considered interchangeable with the next line, until a line that does not start with a question mark (or hash, or colon) is hit (the first line not starting with one will still be considered interchangeable with the lines before). The dependency will be met as long as one or more of the lines belonging to it are available. If the last line in the file starts with a question mark, the dependency will always be considered met and serves no purpose.

-any line starting with an exclamation mark (!) will be considered inverted, useful if your mod conflicts with another mod or must be loaded before that mod. If a line needs both a question mark and an exclamation mark, the question mark must come first.

-any line starting with a colon (:) will be interpreted as an error message and is skipped and gets displayed in case installation is aborted due to an unmet dependency. Only the last stored error message line will be displayed, and an empty error message will not be shown.


# Misc

In modlist.txt, if a line starts with a question mark (?), the mod listed on that line will be considered optional and will be attempted to be installed, but if it is not found, the installer will just skip it and proceed to install the next mod on the list, instead of throwing an error and exiting instantly. Also, the bash script can unzip a zipped mod folder if the folder is missing but a similarly named zip file is present. I thought I should mention these things here instead of the normal readme since they're kinda useless information.
