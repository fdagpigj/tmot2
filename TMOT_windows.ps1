
#file saying which mods to install
$MODLIST="modlist.txt"
#file in a mod saying which other mods it depends on
$DEPENDS="requires.txt"
#file in a mod saying which dependencies this mod will fulfill
$PROVIDES="provides.txt"
#directory containing files that will overwrite any such existing files
$OVERWRITES="overwrites/"
#directory containing files that will be appended to any such existing files
$APPENDS="appends/"
#directory for backups of all modified files
$BACKUPS="backups/"
#directory wherein each mod is a subfolder
$MODS="mods/"
#actual game directory
$ORIG="../"
#version info
$PACKAGE_JSON="package.json"
$VERSION_INFO="recommended_version.txt"

echo "Running..."

if (! (Test-Path "$BACKUPS")) {
    mkdir "$BACKUPS"
} else {
    #restore backups
    if ( $(ls "$BACKUPS") -ne $null ) {
        echo "Restoring old backups..."
        #Get-ChildItem -Path "$BACKUPS" -Recurse |
        #    Copy-Item -Destination "$ORIG"
        Get-ChildItem "$BACKUPS" |
            Copy -Destination "$ORIG" -Recurse -Force
        echo "Done restoring backups."
    }
}

if (! (Test-Path "$MODLIST")) {
    echo "$MODLIST not found. Exiting"
}

$det_ver="none"
if (Test-Path "$ORIG$PACKAGE_JSON") {
    # grep
    $det_ver=$(
        Get-Content -Path "$ORIG$PACKAGE_JSON" |
        select-string -Pattern "version")
    $det_ver = $det_ver -replace '.*"version":\s*"([^"]*)".*', '$1'
    echo "Detected game version $det_ver"
}

function create_backup {
    param ($1)
	if (! (Test-Path "$BACKUPS$1")) {
		if (Test-Path "$ORIG$1") {
			echo "backing up $1"
            $dir = Split-Path "$BACKUPS$1"
            if (! (Test-Path $dir)) {
                mkdir $dir
            }
			cp "$ORIG$1" "$dir"
        }# else {
		#	echo "would've backed up $1 but it does not exist"
        #}
    }# else {
	#    echo "would've backed up $1 but it already is backed up"
    #}
}

#associative array of resources provided by mods installed thus far
#..which other mods may depend on
$MODARRAY = @{}

foreach($modname in Get-Content "$MODLIST") {
    #skip lines starting with #
    if ($modname.StartsWith("#")) {
        continue
    }
    #mods prefixed with ? are installed if present
	$required=1
    if ($modname.StartsWith("?")) {
        $required=0
        $modname= $modname -replace '^\?', ''
    }
	echo "attempting to install mod: $modname"
    # check presence - unzipping seems impossible on older windows
	if (! (Test-Path "$MODS$modname")) {
        if ( $required -eq 1 ) {
            echo "Error: mod folder for $modname (not optional) not found. Exiting"
            exit 1
        } else {
            echo "Mod folder for $modname (marked optional) not found. Skipping"
            continue
        }
    }
    # install
    if (Test-Path "$MODS$modname") {
        $modpath = "$MODS$modname/"
        # check version
        if (Test-Path "$modpath$VERSION_INFO") {
            $rec_ver = $(Get-Content -Path "$modpath$VERSION_INFO")
            if ($rec_ver -ne $det_ver) {
                echo "Warning: $modname recommends version $rec_ver but $det_ver was detected!"
            }
        }
        # check depends
        if (Test-Path "$modpath$DEPENDS") {
            $errormsg = ""
            $anyaltmet = 0
            foreach ($reqname in Get-Content "$modpath$DEPENDS") {
                if ($reqname.StartsWith("#")) {
                    continue
                }
                if ($reqname.StartsWith(":")) {
                    $errormsg = $reqname -replace '^:', ''
                    continue
                }
                $alt = 0
                if ($reqname.StartsWith("?")) {
                    $alt = 1
                    $reqname = $reqname -replace '^\?', ''
                }
                $inverse = 0
                if ($reqname.StartsWith("!")) {
                    $inverse = 1
                    $reqname = $reqname -replace '^\!', ''
                }
                $satisfied = 0
                $exists = 0
                if ($MODARRAY.ContainsKey($reqname)) {
                    $exists = 1
                }
                if ($exists -ne $inverse) {
                    $satisfied = 1
                    $anyaltmet = 1
                }
                if ($satisfied -eq 0) {
                    if ($alt -eq 0 -and $anyaltmet -eq 0) {
                        if ($inverse -eq 0) {
                            echo "Error: Requirement $reqname not met. Exiting"
                        } else {
                            echo "Error: Conflicting resource $reqname loaded. Exiting"
                        }
                        if ($errormsg -ne $null) {
                            echo "$errormsg"
                        }
                        exit 3
                    }
                }
                if ($alt -eq 0) {
                    $anyaltmet = 0
                }
            }
        }
        # overwrites
        if (Test-Path "$modpath$OVERWRITES") {
            foreach ($file in Get-ChildItem -Recurse "$modpath$OVERWRITES") {
                if ($file.PSISContainer) {
                    continue
                }
                $pathname = $file.FullName
                $pathname = $pathname -replace '\\', '/'
                $filename = $pathname -replace ".*/$modpath$OVERWRITES", ''
                create_backup $filename
                if (! (Test-Path "$ORIG$filename")) {
                    $dir = Split-Path $filename
                    if (! (Test-PAth "$ORIG$dir")) {
                        mkdir "$ORIG$dir"
                    }
                    New-Item -type file "$ORIG$filename"
                }
                cp -Force "$modpath$OVERWRITES$filename" "$ORIG$filename"
            }
        }
        # appends
        if (Test-Path "$modpath$APPENDS") {
            foreach ($file in Get-ChildItem -Recurse "$modpath$APPENDS") {
                if ($file.PSISContainer) {
                    continue
                }
                $pathname = $file.FullName
                $pathname = $pathname -replace '\\', '/'
                $filename = $pathname -replace ".*/$modpath$APPENDS", ''
                create_backup $filename
                if (! (Test-Path "$ORIG$filename")) {
                    $dir = Split-Path $filename
                    if (! (Test-PAth "$ORIG$dir")) {
                        mkdir "$ORIG$dir"
                    }
                    New-Item -type file "$ORIG$filename"
                }
                cat "$modpath$APPENDS$filename" |
                    Add-Content -Path "$ORIG$filename"
            }
        }
        # add provides to array
        if (! (Test-Path "$modpath$PROVIDES")) {
            echo "Warning: $modname provides nothing!"
        } else {
            foreach ($reqname in Get-Content "$modpath$PROVIDES") {
                if ($reqname.StartsWith("#")) {
                    continue
                }
                $MODARRAY[$reqname] = 1
            }
        }
        echo "finished installing mod: $modname"
    } else {
        echo "Unknown Error"
    }
}

echo "Done. Exiting."
